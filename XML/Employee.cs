﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace XML
{
    [Serializable]
    public class Employee
    {
        [XmlElement(DataType = "string", ElementName = "Name")]
        public string EmployeeName { get; set; }
        public Employee() {}
        public Employee(string employeeName)
        {
            EmployeeName = employeeName;
        }
    }
}
