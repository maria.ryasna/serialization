﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace XML
{
    [Serializable]
    public class Department
    {
        [XmlElement(DataType = "string", ElementName = "Name")]
        public string DepartmentName { get; set; }

        [XmlArray("ListOfEmployees")]
        [XmlArrayItem("EmployeeEntity")]
        public List<Employee> Employees { get; set; }
        public Department() {}
        public Department(string departmentName, List<Employee> employees)
        {
            DepartmentName = departmentName;
            Employees = employees;
        }
    }
}
