﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace XML
{
    internal class Program
    {
        public static string filePath = @"..\..\..\serialized.xml";
        static void Main(string[] args)
        {
            List<Employee> list = new List<Employee>();
            list.Add(new Employee("Bob"));
            list.Add(new Employee("Tom"));
            list.Add(new Employee("Ann"));
            list.Add(new Employee("Jack"));
            list.Add(new Employee("Julia"));
            Department department = new Department("Development", list);

            try
            {
                Console.WriteLine("Department before serialize:");
                ShowDepartment(department);

                SerializeDepartment(department);

                Department deserializedDepartment = DeserializeDepartment();

                Console.WriteLine("Department after serialize:");
                ShowDepartment(deserializedDepartment);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }

        public static void SerializeDepartment(Department department)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Department));
            TextWriter writer = new StreamWriter(filePath);
            serializer.Serialize(writer, department);
            writer.Close();
        }

        public static Department DeserializeDepartment()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Department));
            TextReader reader = new StreamReader(filePath);
            Department department = (Department)serializer.Deserialize(reader);
            reader.Close();
            return department;
        }

        public static void ShowDepartment(Department department)
        {
            Console.WriteLine($"    Department name: {department.DepartmentName}");
            foreach (var emp in department.Employees)
            {
                Console.WriteLine($"        Employee name: {emp.EmployeeName}");
            }
        } 
    }
}
