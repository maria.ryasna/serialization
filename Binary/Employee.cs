﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Binary
{
    [Serializable]
    public class Employee
    {
        public string EmployeeName { get; set; }
        public Employee(string employeeName)
        {
            EmployeeName = employeeName;
        }
    }
}
