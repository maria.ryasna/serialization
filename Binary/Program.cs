﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Binary
{
    internal class Program
    {
        public static string filePath = @"..\..\..\serialized.txt";
        static void Main(string[] args)
        {
            List<Employee> list = new List<Employee>();
            list.Add(new Employee("Bob"));
            list.Add(new Employee("Tom"));
            list.Add(new Employee("Ann"));
            list.Add(new Employee("Jack"));
            list.Add(new Employee("Julia"));
            Department department = new Department("Development", list);

            try
            {
                Console.WriteLine("Department before serialize:");
                ShowDepartment(department);

                SerializeDepartment(department);

                Department deserializedDepartment = DeserializeDepartment();

                Console.WriteLine("Department after serialize:");
                ShowDepartment(deserializedDepartment);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            

            Console.ReadKey();
        }

        public static void SerializeDepartment(Department department)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            using (var fileStream = new FileStream(filePath, FileMode.Append))
            {
                formatter.Serialize(fileStream, department);
            }
        }

        public static Department DeserializeDepartment()
        {
            BinaryFormatter formatter = new BinaryFormatter();

            using (var fileStream = new FileStream(filePath, FileMode.Open))
            {
                return (Department)formatter.Deserialize(fileStream);
            }
        }

        public static void ShowDepartment(Department department)
        {
            Console.WriteLine($"    Department name: {department.DepartmentName}");
            foreach (var emp in department.Employees)
            {
                Console.WriteLine($"        Employee name: {emp.EmployeeName}");
            }
        }
    }
}
