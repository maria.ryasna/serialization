﻿using System;
using System.Runtime.Serialization;

namespace CustomSerialization
{
    [Serializable]
    public class Person : ISerializable
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
        public Person(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString("PersonName");
            Age = info.GetInt32("PersonAge");
        }
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("PersonName", Name, typeof(string));
            info.AddValue("PersonAge", Age, typeof(int));
        }
    }
}
