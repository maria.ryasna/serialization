﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CustomSerialization
{
    public class Program
    {
        public static string filePath = @"..\..\..\serialized.txt";
        static void Main(string[] args)
        {
            try
            {
                Person person = new Person("Tom", 25);

                Console.WriteLine("Person before serialize:");
                Console.WriteLine($"Name: {person.Name}, Age: {person.Age}");

                SerializePerson(person);

                Person deserializedPerson = DeserializePerson();

                Console.WriteLine("Person after serialize:");
                Console.WriteLine($"Name: {deserializedPerson.Name}, Age: {deserializedPerson.Age}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }

        public static void SerializePerson(Person person)
        {
            IFormatter formatter = new BinaryFormatter();

            using (var fileStream = new FileStream(filePath, FileMode.Append))
            {
                formatter.Serialize(fileStream, person);
            }
        }

        public static Person DeserializePerson()
        {
            IFormatter formatter = new BinaryFormatter();

            using (var fileStream = new FileStream(filePath, FileMode.Open))
            {
                return (Person)formatter.Deserialize(fileStream);
            }
        }
    }
}
