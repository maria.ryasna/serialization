﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace JSON
{ 
    [Serializable]
    public class Employee
    {
        [JsonPropertyName("Employee")]
        public string EmployeeName { get; set; }
        public Employee() { }
        public Employee(string employeeName)
        {
            EmployeeName = employeeName;
        }
    }
}
