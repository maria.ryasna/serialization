﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace JSON
{
    internal class Program
    {
        public static string filePath = @"..\..\..\serialized.json";
        static void Main(string[] args)
        {
            List<Employee> list = new List<Employee>();
            list.Add(new Employee("Bob"));
            list.Add(new Employee("Tom"));
            list.Add(new Employee("Ann"));
            list.Add(new Employee("Jack"));
            list.Add(new Employee("Julia"));
            Department department = new Department("Development", list);

            Console.WriteLine("Department before serialize:");
            ShowDepartment(department);

            SerializeDepartment(department);

            Department deserializedDepartment = DeserializeDepartment();

            Console.WriteLine("Department after serialize:");
            ShowDepartment(deserializedDepartment);

            Console.ReadKey();
        }

        public static void SerializeDepartment(Department department)
        {
            var options = new JsonSerializerOptions { WriteIndented = true };
            var jsonString = JsonSerializer.Serialize(department, options);
            File.WriteAllText(filePath, jsonString);
        }

        public static Department DeserializeDepartment()
        {
            var jsonString = File.ReadAllText(filePath);
            return JsonSerializer.Deserialize<Department>(jsonString);
        }

        public static void ShowDepartment(Department department)
        {
            Console.WriteLine($"    Department name: {department.DepartmentName}");
            foreach (var emp in department.Employees)
            {
                Console.WriteLine($"        Employee name: {emp.EmployeeName}");
            }
        }
    }
}
