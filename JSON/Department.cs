﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace JSON
{
    [Serializable]
    public class Department
    {
        [JsonPropertyName("Department")]
        public string DepartmentName { get; set; }
        [JsonPropertyName("ListOfEmployees")]
        public List<Employee> Employees { get; set; }
        public Department() { }
        public Department(string departmentName, List<Employee> employees)
        {
            DepartmentName = departmentName;
            Employees = employees;
        }
    }
}
