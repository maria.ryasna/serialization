﻿using System;
using System.Collections.Generic;

namespace DeepCloning
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Employee> list = new List<Employee>();
            list.Add(new Employee("Bob"));
            list.Add(new Employee("Tom"));
            list.Add(new Employee("Ann"));
            list.Add(new Employee("Jack"));
            list.Add(new Employee("Julia"));
            Department department = new Department("Development", list);

            Console.WriteLine("Department before cloning:");
            ShowDepartment(department);

            Department otherDepartment = (Department)department.Clone();
            otherDepartment.DepartmentName = "Design";
            otherDepartment.Employees.RemoveAt(0);
            otherDepartment.Employees.RemoveAt(1);
            otherDepartment.Employees[0].EmployeeName = "John";
            otherDepartment.Employees.Add(new Employee("Rick"));

            Console.WriteLine("New department after cloning and changes:");
            ShowDepartment(otherDepartment);

            Console.WriteLine("Previous department after cloning and changes new department:");
            ShowDepartment(department);

            Console.ReadKey();
        }

        public static void ShowDepartment(Department department)
        {
            Console.WriteLine($"    Department name: {department.DepartmentName}");
            foreach (var emp in department.Employees)
            {
                Console.WriteLine($"        Employee name: {emp.EmployeeName}");
            }
        }
    }
}
